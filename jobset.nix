{ random ? "42"
} : { trivial = builtins.derivation {
    name = "trivial";
    system = "x86_64-linux";
    builder = "/bin/sh";
    allowSubstitutes = false;
    preferLocalBuild = true;
    args = ["-c" "echo success $random > $out; exit 0"];
  };
}
