{ nixpkgs, pulls, ... }:

let
  pkgs = import nixpkgs {};
  pulls_ = builtins.fromJSON (builtins.readFile pulls);

  repo_uri = "https://gitlab.com/Amerocu/hydra-test.git";

  mkPull = pull: let
      id = "PR-" + pull.iid;
      job = {
        description = "PR ${pull.iid}: ${pull.title}";
        checkinterval = "30";
        enabled = "1";
        schedulingshares = 100;
        enableemail = false;
        emailoverride = "";
        nixexprinput = "gitlab";
        nixexprpath = ".hydra/hydra.nix";
        type = 0;
        keepnr = 3;
        hidden = false;
        inputs = {
          gitlab = {
            value = "${repo_uri} merge-requests/${pull.iid}/head";
            type = "git";
            emailresponsible = false;
          };
        };
      };
    in { name = id; value = job; };

  mkBranch = branch: let
      id = "BR-" + branch;
      job = {
        description = "Build ${branch} branch";
        checkinterval = "60";
        enabled = "1";
        schedulingshares = 100;
        enableemail = false;
        emailoverride = "";
        nixexprinput = "gitlab";
        nixexprpath = ".hydra/hydra.nix";
        type = 0;
        keepnr = 3;
        hidden = false;
        inputs = {
          gitlab = {
            value = "${repo_uri} ${branch}";
            type = "git";
            emailresponsible = false;
          };
        };
      };
    in { name = id; value = job; };

  jobsets = builtins.listToAttrs
      ( [
        (mkBranch "main")
      ] 
      ++ (builtins.map (pull: mkPull pull) (builtins.attrValues pulls_))
    );

  log = {
    pulls = pulls_;
    jobsets = jobsets;
  };

in {
  jobsets = pkgs.runCommand "spec-jobsets.json" {} ''
    cat >$out <<EOF
    ${builtins.toJSON jobsets}
    EOF
    # This is to get nice .jobsets build logs on Hydra
    cat >tmp <<EOF
    ${builtins.toJSON log}
    EOF
    ${pkgs.jq}/bin/jq . tmp
  '';
}
